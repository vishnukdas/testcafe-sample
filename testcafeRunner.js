  const createTestCafe = require('testcafe');

  const browsers = [
    ["lambdatest:Google Pixel 2@9:android",
     "lambdatest:Chrome@89.0:Windows 10","lambdatest:Google Pixel 2@9:android",
     "lambdatest:Chrome@89.0:Windows 10","lambdatest:Google Pixel 2@9:android",
     "lambdatest:Chrome@89.0:Windows 10"
          ],
  ];
  const runTest = async browser => {
    console.log('starting tests');
    await createTestCafe('localhost')
      .then(tc => {
        testcafe = tc;
        const runner = testcafe.createRunner();
        return runner
          .src(['test.js'])
          .browsers(browser)
          .run();
      })
      .then(async failedCount => {
        console.log('Tests failed: ' + failedCount);
        await testcafe.close();
        return;
      });
  }
  const runAllBrowsers = async () => {
    for (const browser of browsers) {
      await runTest(browser);
    }
  }
  runAllBrowsers();
